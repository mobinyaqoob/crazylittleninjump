﻿using UnityEngine;
using System.Collections;
using Heyzap;

public class ShowAds : MonoBehaviour {

    public static ShowAds instance = null;

    void Awake()
    {

        if (instance == null)

            instance = this;

        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }

//    // Use this for initialization
    void Start()
    {
        //HeyzapAds.Start("8f25340136433f655e3493f5c1618ad9", HeyzapAds.FLAG_NO_OPTIONS);  //Dubai Games Studio
        HeyzapAds.Start("407f08d6aff8354784115adb136f754f", HeyzapAds.FLAG_NO_OPTIONS);    //Mobit Solutions
        //f5841fcc88750e7affab9e45d8063208e8f2bca3d2a7417c4f9e15f814913ad6
        HZInterstitialAd.Fetch();
#if UNITY_ANDROID
//        HZBannerShowOptions showOptions = new HZBannerShowOptions();
////        showOptions.Position = HZBannerShowOptions.POSITION_TOP;
//        showOptions.Position = HZBannerShowOptions.;
//        HZBannerAd.ShowWithOptions(showOptions);


        HZBannerShowOptions showOptions = new HZBannerShowOptions();
        showOptions.Position = HZBannerShowOptions.POSITION_BOTTOM;
        showOptions.SelectedAdMobSize = HZBannerShowOptions.AdMobSize.BANNER; // optional, android only
        showOptions.SelectedFacebookSize = HZBannerShowOptions.FacebookSize.BANNER_HEIGHT_50; // optional, android only
        HZBannerAd.ShowWithOptions(showOptions);


#endif
 
     //   ShowInter();
        ShowBanner();
        FetchRewardedVideo();
    }



    public void ShowInter()
    {
        StartCoroutine(ShowInterstecialAd());
    }

    public void FetchVideo()
    {
     //   HZVideoAd.Fetch();
    }
    public void ShowBanner()
    {


        HZBannerShowOptions showOptions = new HZBannerShowOptions();
        showOptions.Position = HZBannerShowOptions.POSITION_TOP;
        showOptions.SelectedAdMobSize = HZBannerShowOptions.AdMobSize.BANNER; // optional, android only
        showOptions.SelectedFacebookSize = HZBannerShowOptions.FacebookSize.BANNER_HEIGHT_50; // optional, android only
        HZBannerAd.ShowWithOptions(showOptions);


        //HZBannerShowOptions showOptions = new HZBannerShowOptions();
        //showOptions.Position = HZBannerShowOptions.POSITION_TOP;
        //HZBannerAd.ShowWithOptions(showOptions);
    }
    public void ShowVideoAds()
    {
        StartCoroutine(ShowVideoAdsCoro());
    }
    IEnumerator ShowVideoAdsCoro()
    {
        yield return new WaitForSeconds(3f);
        //if (HZVideoAd.IsAvailable())
        //{
        //    HZVideoAd.Show();
        //}
    }
    IEnumerator ShowInterstecialAd()
    {
        yield return new WaitForSeconds(0f);

        if (HZInterstitialAd.IsAvailable())
        {
            HZInterstitialAd.Show();
        }
        //  HeyzapAds.ShowMediationTestSuite();
    }



    public void FetchRewardedVideo()
    {
        HZIncentivizedAd.Fetch();
    }


    public void ShowRewardedVideo()
    {
        StartCoroutine(Co_ShowRewardedVideo());        
    }

    IEnumerator Co_ShowRewardedVideo()
    {
        yield return new WaitForSeconds(3f);

        //if (HZIncentivizedAd.IsAvailable())
        //{
        //    HZIncentivizedAd.Show();


        //    HZIncentivizedAd.AdDisplayListener listener = delegate(string adState, string adTag)
        //    {
        //        if (adState.Equals("incentivized_result_complete"))
        //        {
        //            GameManagerOld.Instance.onAdComplete = "Ad Completed";
        //            // The user has watched the entire video and should be given a reward.
        //        }
        //        if (adState.Equals("incentivized_result_incomplete"))
        //        {
        //            GameManagerOld.Instance.onAdNotComplete = "Ad Not Completed";

        //            // The user did not watch the entire video and should not be given a   reward.
        //        }
        //    };

        //    HZIncentivizedAd.SetDisplayListener(listener);


        }

    public void ShowMediationSuite()
    {
        HeyzapAds.ShowMediationTestSuite();
    }
    }


